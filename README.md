## Projects for week two of the Graduate Boot Camp

This repo contains the Java and Spring projects to be used on the second week of the Graduate Boot Camp. 

### These are the Java projects from the *JavaProjects* subfolder

| Folder Name             | Description                                  | 
| ----------------------- |----------------------------------------------| 
| JavaLanguageExamples    |	Examples of the Java language                |
| JavaLanguageExercises	  | Exercises for learning Java                  |
| JavaNewFeatures		  | Features added from Java V9-V11              |
| JavaThreading		      | Examples of the Java threading libraries     |
| CompletableFutureDemo	  | A dedicated project for *CompletableFuture*  |
| JmhDemos                | Creating benchmarks using the JMH tool       |

### These are the Spring projects from the *SpringProjects* subfolder

| Folder Name                                         | Description                                                      |  
| --------------------------------------------------- |------------------------------------------------------------------|
| SpringMVC/DefaultUserLogin                          | Configuring Spring Security with an in-memory DB                 |
| SpringMVC/ConfigurationDemo                         | Injecting configuration values into Spring Beans                 |
| SpringMVC/FlightBookingApp                          | A simple old-school Spring MVC Web Application                   |
| SpringMVC/SpringMvcBasics                           | Examples of writing *@Controller* beans for Web Apps             |
| SpringMVC/FlightBookingChaosServer                  | A RESTful service with latency added by Chaos Monkey             |
| SpringMVC/RestfulServicesWithSpringBoot             | A sample service written in three separate styles                |
| SpringMVC/TestingRestfulServices                    | Using the Spring Boot test framework for services                |
| SpringMVC/MonitoringServicesWithActuator            | Using Actuator to generate metrics around a service              |
| SpringCore/AspectOrientedCodingInSpring             | Intercepting calls to specified beans via Spring AOP             |
| SpringCore/MarkupBasedExercises                     | Exercises on XML based dependency injection                      |
| SpringCore/AnnotationBasedExercises                 | Exercises on annotation based dependency injection               |
| SpringCore/GroovyDrivenDI                           | Performing dependency injection using the Groovy DSL             |
| SpringCore/UnitTestingSpringComponents              | Testing Spring Beans using the extensions to JUnit               |
| SpringCore/MarkupDrivenDI                           | Performing dependency injection using XML based wiring           |
| SpringCore/AnnotationDrivenDI                       | Performing dependency injection using annotations                |
| ProjectReactor/ProjectReactorExamples               | Basic demos of reactive coding with *Flux* and *Mono*            |
| SpringWebFlux/SpringWebFluxJavaObjectsFirst         | Creating REST services in WebFlux via annotations                |
| SpringWebFlux/SpringWebFluxWithCoroutines           | Writing WebFlux controllers using *suspend* and *flow*           |
| SpringWebFlux/SpringWebFluxKotlinRedis              | Integrating WebFlux controllers with a NoSQL Database            |
| SpringWebFlux/SpringWebFluxMinimalist               | Creating services on Netty with as little code as possible       |
| SpringWebFlux/SpringWebFluxJavaFunctionsFirst       | Creating services in Java as Functional Endpoints                |
| SpringWebFlux/ClientForSpringServices               | A JavaFX / TornadoFX client for the WebFlux demo                 |
| SpringWebFlux/SpringWebFluxKotlinObjectsFirst       | Creating services in WebFlux via Kotlin and annotations          |
| SpringWebFlux/SpringWebFluxKotlinFunctionsFirst     | Creating services in WebFlux via Kotlin as functional endpoints  |
| SpringWebFlux/SpringWebFluxJavaRedis                | Integrating WebFlux controllers with a NoSQL Database in Kotlin  |
| SpringData/SpringDataJpaDemos                       | Demos of creating Spring Data JPA based repositories             |
| SpringData/JdbcTemplateBasics                       | Using the Spring *JdbcTemplate*                                  |
| Extras/ServicesInHttp4k                             | The 'Hello World' of the Http4k REST framework                   |
| Extras/TornadoFxDemos                               | Creating JavaFX UIs using the Kotlin TornadoFX DSL               |


